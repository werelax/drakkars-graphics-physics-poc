(function () {
  "use strict";

  function tick(world, body) {
    var results = world.boats.map(checkBoatColision.bind(this, body));
    results.forEach(function(result) {
      if (result.collision) {
        body = solveCollision(body, result.response);
      }
    });

    //TODO Miguel: check with islands on a close radio only (x axis)
    results = world.islands.map(checkIslandColision.bind(this, body));
    results.forEach(function(result) {
      if (result.collision) {
        body = solveCollision(body, result.response);
      }
    });
    //TODO Miguel: check with bankshapes on a close radio only (x axis)
    results = world.leftBank.map(checkIslandColision.bind(this, body));
    results.forEach(function(result) {
      if (result.collision) {
        body = solveCollision(body, result.response);
      }
    });
    //TODO Miguel: check with bankshapes on a close radio only (x axis)
    results = world.rightBank.map(checkIslandColision.bind(this, body));
    results.forEach(function(result) {
      if (result.collision) {
        body = solveCollision(body, result.response);
      }
    });
    return body;
  }

  function checkBoatColision(body, againstBody) {
    var response = new SAT.Response(),
        collision;
    if (body === againstBody) {return {collision: false, response: response}}
        collision = SAT.testCircleCircle(body.shape, againstBody.shape, response);
    return {collision: collision, response: response};
  }

  function checkIslandColision(body, againstBody) {
    var response = new SAT.Response(),
        collision = SAT.testCirclePolygon(body.shape, againstBody.shape.toPolygon(), response);
    return {collision: collision, response: response};
  }

  function solveCollision(body, response) {
    body.shape.pos = {x: correct(body.shape.pos.x - response.overlapV.x), y: correct(body.shape.pos.y - response.overlapV.y)};
    body =  bounce(body, response);
    body = physics.reverseAngularAcceleration(body);
    body = physics.stopBody(body);
    return body;
  }

  function bounce(body, response) {
    var colisionVector = calculateColisionVector(body, response.b),
        angle = calculateRefractionAngle(body, colisionVector);
    physics.rotate(body,angle);
    return body;
  }

  function calculateRefractionAngle(body, colisionVector) {
    var angle = Math.atan2(Math.sin(body.direction), Math.cos(body.direction)) - Math.atan2(colisionVector.y,  colisionVector.x) % Math.PI*2,
        opositeAngle = Math.PI - 2*angle;
    return colisionVector.y === 0 ? Math.PI + opositeAngle : opositeAngle;
  }

  function calculateColisionVector(body, polygon) {
    var edges = polygon.edges || [],
        calcPoints = absolutePoints(polygon) || [],
        colisionVector= edges.length !== 0 ? edges[0] : {x: 1, y:0},
        minDistance = edges.length !== 0 ? distToSegment(body.shape.pos, calcPoints[0], calcPoints[(1)]) : 0,
        distance;
    edges.forEach(function(edge, index) {
      distance = distToSegment(body.shape.pos, calcPoints[index], calcPoints[(index + 1) % calcPoints.length ])
      if (distance < minDistance) {
        minDistance = distance;
        colisionVector = edge;
       }
    })
    return colisionVector;
  }

  function absolutePoints(polygon) {
    if (!polygon.calcPoints) {return []}
    return polygon.calcPoints.map(function(calcPoint) {
      return {x: polygon.pos.x + calcPoint.x, y: polygon.pos.y + calcPoint.y};
    });
  }

  function sqr(x) { return x * x }
  function dist2(v, w) { return sqr(v.x - w.x) + sqr(v.y - w.y) }
  function distToSegmentSquared(p, v, w) {
    var l2 = dist2(v, w);
    if (l2 == 0) return dist2(p, v);
    var t = ((p.x - v.x) * (w.x - v.x) + (p.y - v.y) * (w.y - v.y)) / l2;
    if (t < 0) return dist2(p, v);
    if (t > 1) return dist2(p, w);
    return dist2(p, { x: v.x + t * (w.x - v.x),
                      y: v.y + t * (w.y - v.y) });
  }
  function distToSegment(p, v, w) { return Math.sqrt(distToSegmentSquared(p, v, w)); }


  window.colisions = {
    tick: tick
  };
}());
