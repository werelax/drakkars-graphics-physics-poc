(function() {
  'use strict';

  function initStats() {
    var stats = new Stats();
    stats.setMode(1); // 0: fps, 1: ms
    stats.domElement.style.position = 'absolute';
    stats.domElement.style.right = '0px';
    stats.domElement.style.top = '0px';
    document.body.appendChild(stats.domElement);
    return stats;
  }

  function createCanvas() {
    var canvas = document.createElement('canvas');
    canvas.width = window.innerWidth;
    canvas.height = window.innerHeight;
    document.body.appendChild(canvas);
    return canvas;
  }


  function onFrame(screen, world, stats) {
    // config values
    var now, delta, last;
    var msPerFrame = 1000/60;
    var boat = game.getBoat(world, 0);

    // time scalar values

    return function frameTick() {
      stats.begin();
      now = new Date();
      delta = (now - last) / msPerFrame;

      // magic here

      if (input.isKeyPressed(input.keys.UP)) {
        boat = game.thrustBoat(boat);
      }
      if (input.isKeyPressed(input.keys.LEFT)) {
        boat = game.rotateBoat(boat, game.rows.LEFT);
      } else if (input.isKeyPressed(input.keys.RIGHT)) {
        boat = game.rotateBoat(boat, game.rows.RIGHT);
      }
      if (input.isKeyPressed(input.keys.A)) {
        boat = game.rowBoat(boat, game.rows.LEFT);
      }
      if (input.isKeyPressed(input.keys.D)) {
        boat = game.rowBoat(boat, game.rows.RIGHT);
      }

      world = game.tick(world);

      graphics.clearScreen(screen);
      graphics.drawWorld(screen, world);
      //graphics.drawBoat(screen, boat);
      // boat = game.rotateBoat(boat, 0.1);

      stats.end();
      last = now;
      // and again...
      window.reqFrame(frameTick);
    };
  }

  function onLoad() {
    var canvas = createCanvas(),
        screen = graphics.createScreen(canvas),
        world = game.createWorld(screen),
        stats = initStats();
    input.init();

    world = game.addBoat(world,
                         game.createBoat(10, (screen.w/2)<<0, (screen.h/2)<<0));
    world = game.addBoat(world,
                         game.createBoat(10, (screen.w/2 + 40)<<0, (screen.h/2)<<0));

    world = game.addBanks(world,
                         game.createBank(world.leftBank, screen), game.createBank(world.rightBank, screen));
    world = game.addIsland(world,
                         game.createIsland({position: {x: (screen.w/4)<<0, y: (screen.h/4)<<0}, width: 30, height: 30}));
    // poly filling..
    window.reqFrame = window.requestAnimationFrame ||
      window.mozRequestAnimationFrame ||
      webkitRequestAnimationFrame;
    // start the loop
    window.reqFrame(onFrame(screen, world, stats));
  }

  window.addEventListener('load', onLoad);
}());
