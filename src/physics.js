(function () {
  "use strict";

  var constants = {
    acceleration: 1,
    angularAcceleration: 0.01,
    friction: 0.2,
    angularFriction: 10
  };

  function tick(body) {
    // apply frictions, accelerations and then velocities
    var velocity = body.movement.linear.velocity,
        speed = Math.sqrt(velocity.x*velocity.x + velocity.y*velocity.y),
        angle = Math.atan2(velocity.y, velocity.x); // angle of velocity != direction of body
    // linear friction
    body = applyLinearForce(body, angle-Math.PI, constants.friction * speed);
    // angular friction
    body = applyAngularForce(body, constants.angularFriction *
                             (-1*body.movement.angular.velocity));
    // accelerations
    body = applyLinearAcceleration(body);
    body = applyAngularAcceleration(body);
    // movement
    body = move(body, body.movement.linear.velocity);
    body = rotate(body, body.movement.angular.velocity);
    return body;
  }

  function move(body, velocity) {
    // apply linear velocity
    body.shape.pos = {
      x: body.shape.pos.x + velocity.x,
      y: body.shape.pos.y + velocity.y
    };
    return body;
  }

  function rotate(body, angle) {
    // apply angular velocity
    body.direction += angle;
    return body;
  }

  function applyLinearAcceleration(body) {
    // apply acceleration to speed
    var speed = body.movement.linear.velocity,
        acceleration = body.movement.linear.acceleration;
    body.movement.linear.velocity = {
      x: correct(speed.x + acceleration.x),
      y: correct(speed.y + acceleration.y)
    };
    body.movement.linear.acceleration = {x: 0, y: 0};
    return body;
  }

  function reverseAngularAcceleration(body) {
    body.movement.linear.acceleration.x = - body.movement.linear.acceleration.x;
    body.movement.linear.acceleration.y = - body.movement.linear.acceleration.y;
    return body;
  }

  function stopBody(body) {
    body.movement.linear.velocity = {x: 0, y: 0};
    return body;
  }

  function applyAngularAcceleration(body) {
    // apply angularAcceleration to angularVelocity
    var angularSpeed = body.movement.angular.velocity,
        angularAcceleration = body.movement.angular.acceleration;
    body.movement.angular.velocity = correct(angularSpeed + angularAcceleration);
    body.movement.angular.acceleration = 0;
    return body;
  }

  function applyLinearForce(body, direction, magnitude) {
    // adds to acceleration
    // TODO: Inertia pending
    var movement = body.movement.linear,
        prevAccel = movement.acceleration,
        forceVector = {x: magnitude*cos(direction), y: magnitude*sin(direction)};
    body.movement.linear.acceleration = {
      x: prevAccel.x + forceVector.x * constants.acceleration,
      y: prevAccel.y + forceVector.y * constants.acceleration
    };
    return body;
  }

  function applyAngularForce(body, angularForce) {
    // adds to angular acceleration
    var prevAccel = body.movement.angular.acceleration;
    body.movement.angular.acceleration = prevAccel +
      angularForce * constants.angularAcceleration;
    return body;
  }

  window.physics = {
    tick: tick,
    rotate: rotate,
    move: move,
    applyLinearForce: applyLinearForce,
    applyAngularForce: applyAngularForce,
    reverseAngularAcceleration: reverseAngularAcceleration,
    stopBody: stopBody
  };
}());
