(function() {
  "use strict";

  var pressedKeys = {};

  function init() {
    window.addEventListener("keydown", onkeydown, false);
    window.addEventListener("keyup", onkeyup, false);
  }

  function onkeydown(e) {
    pressedKeys[e.keyCode] = true;
  }

  function onkeyup(e) {
    pressedKeys[e.keyCode] = false;
  }

  function isKeyPressed(keyCode) {
    return pressedKeys[keyCode];
  }

  function getPressedKeys() {
    return Object.create(pressedKeys);
  }

  window.input = {
    init: init,
    isKeyPressed: isKeyPressed,
    getPressedKeys: getPressedKeys,
    keys: {
      LEFT: 37,
      RIGHT: 39,
      UP: 38,
      DOWN: 40,
      W: 87,
      A: 65,
      S: 83,
      D: 68
    }
  };
}());
