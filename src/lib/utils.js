function extend() {
  "use stric";
  return Array.prototype.reduce.call(arguments, function(acc, el) {
    for (var k in el) if (el.hasOwnProperty(k)) acc[k] = el[k];
    return acc;
  });
}

function correct(n) {
  // just remove small imprecissions by rounding to 0
  // (without this method, small imprecisions would add up and
  // the friction force will start pushing backwards!)
  return (Math.abs(n) < 0.01)? 0 : n;
}

function klass(parent, props) {
  "use strict";
  var F = (props && props.init) || function() { return parent.apply(this, arguments); };
  F.prototype = extend(Object.create(parent.prototype), props);
  F._super = F.prototype._super = parent;
  return F;
}

window.sin = Math.sin;
window.cos = Math.cos;

window._180oPI = 180 / Math.PI;

function d2r(deg) {
  return deg / window._180oPI;
}

function r2d(radians) {
  return radians * window._180oPI;
}

function rand(n) {
  return (0.5 + (Math.random() * n)) << 0;
}

function getRandomInt(min, max) {
  return Math.floor(Math.random() * (max - min)) + min;
}



function rgba(r, g, b, a) {
  return "rgba(" + [r, g, b, a].join(", ") + ")";
}
