(function() {
  "use strict";

  function createScreen(canvas) {
    return {
      canvas: canvas,
      ctx: canvas.getContext('2d'),
      w: canvas.width,
      h: canvas.height
    };
  }

  function clearScreen(screen) {
    screen.ctx.clearRect(0, 0, screen.w, screen.h);
  }

  function drawWorld(screen, world) {
    world.islands.map(drawIsland.bind(this, screen));
    world.boats.map(drawBoat.bind(this, screen));
    world.leftBank.map(drawBank.bind(this, screen));
    world.rightBank.map(drawBank.bind(this, screen));
  }

  function drawBoat(screen, boat) {
    screen.ctx.strokeStyle = boat.graphics.color;
    screen.ctx.beginPath();
    screen.ctx.arc(boat.shape.pos.x, boat.shape.pos.y, boat.shape.r, 0, 2*Math.PI, false);
    screen.ctx.moveTo(boat.shape.pos.x, boat.shape.pos.y);
    screen.ctx.lineTo(boat.shape.pos.x + boat.shape.r*cos(boat.direction),
                      boat.shape.pos.y + boat.shape.r*sin(boat.direction));
    screen.ctx.stroke();
  }

  function drawIsland(screen, island) {
    screen.ctx.fillRect(island.shape.pos.x, island.shape.pos.y, island.shape.w, island.shape.h);
  }

  function drawBank(screen, bank) {
    screen.ctx.fillRect(bank.shape.pos.x, bank.shape.pos.y, bank.shape.w, bank.shape.h);
  }

  window.graphics = {
    createScreen: createScreen,
    clearScreen: clearScreen,
    drawWorld: drawWorld
  };
}());
