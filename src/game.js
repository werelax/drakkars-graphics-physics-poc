(function() {
  "use strict";

  function createWorld(screen) {
    return {
      boats: [],
      islands: [],
      leftBank: [createStartingLeftBank(screen)],
      rightBank: [createStartingRightBank(screen)],
      ticks: 0,
      state: 0
    };
  }

  var constraints = {
    maxRiverCurve: 10,
    maxDxRiverCurve: 20,
    startingWidth: 50,
    riverShapeHeight: 10
  };

  function createBank(bank, screen) {
    for (var i = 0; i < screen.h; i++) {
      bank.push(createBankShape(bank[bank.length - 1], screen));
    }
    return bank;
  }

  function createStartingLeftBank(screen) {
    return {
      shape: new SAT.Box(new SAT.Vector(0,0), constraints.startingWidth, constraints.riverShapeHeight),
      graphics: {
        color: rgba(rand(255), rand(255), rand(255), 1)
      }
    };
  }

  function createStartingRightBank(screen) {
    return {
      shape: new SAT.Box(new SAT.Vector(screen.w - constraints.startingWidth, 0), constraints.startingWidth, constraints.riverShapeHeight),
      graphics: {
        color: rgba(rand(255), rand(255), rand(255), 1)
      }
    }
  }

  function createBankShape(previous, screen) {
    var bankWidth = getRandomInt(previous.shape.w - constraints.maxDxRiverCurve, previous.shape.w + constraints.maxDxRiverCurve),
        xAxis;
        bankWidth = bankWidth >= 1 ? bankWidth : 1;
        xAxis = previous.shape.pos.x === 0 ? 0 : screen.w - bankWidth;
    return {
      shape: new SAT.Box(
        new SAT.Vector(xAxis, previous.shape.pos.y + constraints.riverShapeHeight),
        bankWidth,
        constraints.riverShapeHeight
        ),
      graphics: {
        color: rgba(rand(255), rand(255), rand(255), 1)
      }
    }
  }

  function createIsland(options) {
    return {
      shape: new SAT.Box(new SAT.Vector(options.position.x, options.position.y), options.width, options.height),
      graphics: {
        color: rgba(rand(255), rand(255), rand(255), 1)
      }
    };
  }

  function createBoat(radius, x, y) {
    return {
      movement: {
        linear: {velocity: {x: 0, y: 0}, acceleration: {x: 0, y: 0}},
        angular: {velocity: 0, acceleration: 0},
      },
      shape: new SAT.Circle(new SAT.Vector(x,y), radius),
      direction: 0.7,
      graphics: {
        color: rgba(rand(255), rand(255), rand(255), 1)
      }
    };
  }

  function tick(world) {
    world.boats = world.boats.map(physics.tick);
    // phisics.tick for every body
    // collysions
    world.boats = world.boats.map(colisions.tick.bind(this, world));
    // game conditions
    return world;
  }

  function rotateBoat(boat, direction) {
    return physics.applyAngularForce(boat, direction);
  }

  function thrustBoat(boat) {
    return physics.applyLinearForce(boat, boat.direction, 1);
  }

  function rowBoat(boat, direction) {
    boat = thrustBoat(boat);
    return physics.applyAngularForce(boat, direction);
  }

  function addBoat(world, boat) {
    world.boats.push(boat);
    return world;
  }

  function addBanks(world, leftBank, rightBank) {
    world.rightBank = rightBank;
    //world.leftBank = leftBank;
    return world;
  }

  function addIsland(world, island) {
    console.log(island);
    world.islands.push(island)
    return world;
  }

  function getBoat(world, at) {
    return world.boats[at];
  }

  window.game = {
    createWorld: createWorld,
    createBoat: createBoat,
    createIsland: createIsland,
    createBank: createBank,
    tick: tick,
    rotateBoat: rotateBoat,
    thrustBoat: thrustBoat,
    rowBoat: rowBoat,
    addBoat: addBoat,
    addIsland: addIsland,
    addBanks: addBanks,
    getBoat: getBoat,
    rows: {
      LEFT: -1,
      RIGHT: 1
    }
  };
}());
